package ConnectFour;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class EndPanel extends Panel{
    private JLabel banner;
    private JLabel restartLabel;
    private JLabel changeModeLabel;
    private JButton restart;
    private JButton changeMode;
    private FontMetrics fontMetrics;

    private int gridBagContraintsCounter;

    public EndPanel (int width, int height, final Controller controller) {
        super(width, height, controller);
        this.setLayout(new GridBagLayout());
        this.setBackground(Color.orange);
        this.setBorder(BorderFactory.createLoweredBevelBorder());
        this.setOpaque(true);

        // Initialize the buttons and labels
        this.banner = new JLabel("Hello World");
        this.restartLabel = new JLabel("Restart the game");
        this.changeModeLabel = new JLabel("Change Game Mode");
        this.restart = new JButton("Restart");
        this.changeMode = new JButton("Change Mode");

        // Add the ActionListener to the buttons
        this.restart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.startGame();
            }
        });
        this.changeMode.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.setGameMode();
            }
        });

        // Some text formatting
        this.banner.setFont(banner.getFont().deriveFont(35.0f).deriveFont(Font.BOLD));
        this.restartLabel.setFont(restart.getFont().deriveFont(16.0f).deriveFont(Font.BOLD));
        this.changeModeLabel.setFont(changeModeLabel.getFont().deriveFont(16.0f).deriveFont(Font.BOLD));
        this.banner.setHorizontalAlignment(SwingConstants.CENTER);
        this.restartLabel.setHorizontalAlignment(SwingConstants.CENTER);
        this.changeModeLabel.setHorizontalAlignment(SwingConstants.CENTER);
        this.restart.setHorizontalAlignment(SwingConstants.CENTER);
        this.changeMode.setHorizontalAlignment(SwingConstants.CENTER);

        this.fontMetrics = banner.getFontMetrics(banner.getFont());

        // Align the buttons and labels along a GridBagLayout
        this.gridBagContraintsCounter = 0;

        this.add(banner, getGridBagContraint(new Insets(30,0,10,0)));
        this.add(restartLabel, getGridBagContraint(new Insets(0,0,5,0)));
        this.add(restart, getGridBagContraint(new Insets(0,0,5,0)));
        this.add(changeModeLabel, getGridBagContraint(new Insets(0,0,5,0)));
        this.add(changeMode, getGridBagContraint(new Insets(0,0,20,0)));
    }

    public void setBannerText(String text) {
        if (fontMetrics.stringWidth(text) >= width) {
            float size = width / text.length();
            banner.setFont(getFont().deriveFont((float) size * 2));
        }
        banner.setText(text);
    }
    public void setBannerColor(Color c) {
        banner.setForeground(c);
    }

    private GridBagConstraints getGridBagContraint(Insets insets) {
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = insets;
        c.gridwidth = 3;
        c.gridx = 1;
        c.gridy = gridBagContraintsCounter;
        gridBagContraintsCounter++;
        return c;
    }

}

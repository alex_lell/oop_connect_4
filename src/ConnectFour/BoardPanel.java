package ConnectFour;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.IOException;

public class BoardPanel extends Panel {
    private Board board;
    private Image backgroundImage;
    private Image redDiskImage;
    private Image yellowDiskImage;
    private Disk redDisk;
    private Disk yellowDisk;
    private Disk currentDisk;

    private int diskDiameter;
    private int padding;
    private int columnSeparator;
    private int backgroundPaddingTop;
    private int backgroundHeight;
    private int fps = 30;
    private int distance;
    private int increment = 15;
    private Timer timer;
    private boolean animationHasFinished = false;

    public BoardPanel(int width, int height, Controller controller) {
        super(width, height, controller);
        this.board = controller.getBoard();
        this.diskDiameter = controller.getDiskDiameter();
        this.padding = controller.getPadding();
        this.columnSeparator = controller.getColumnSeparator();

        // The ratio of the board image is 800 to 684 pixels.
        // We make sure that this ratio is kept when drawing the board on the panel.
        this.backgroundHeight = Math.round(((float) this.width / 1000) * 855);
        this.backgroundPaddingTop = this.height - this.backgroundHeight;

        // Create the background and disk objects.
        this.setBackground(new Color(255, 255, 190));
        try {
            this.backgroundImage = ImageIO.read(getClass().getResourceAsStream("/ConnectFour/Images/bgImg.png"));

            this.redDiskImage = ImageIO.read(getClass().getResourceAsStream("/ConnectFour/Images/redDisk.png"));
            this.redDisk = new Disk(redDiskImage, padding, 15, diskDiameter);

            this.yellowDiskImage = ImageIO.read(getClass().getResourceAsStream("/ConnectFour/Images/yellowDisk.png"));
            this.yellowDisk = new Disk(yellowDiskImage, padding, 15, diskDiameter);

            this.currentDisk = redDisk;
        } catch (IllegalComponentStateException e) {
            System.err.println("The input was null or illegal!");
            e.printStackTrace();
        } catch (IOException e) {
            System.err.println("Loading the images of BoardPanel went wrong.");
            e.printStackTrace();
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(currentDisk.getImage(), currentDisk.getX(), currentDisk.getY(), diskDiameter, diskDiameter, this);
        drawModel(g);
        g.drawImage(backgroundImage, 0, backgroundPaddingTop, width, backgroundHeight, this);
    }

    public Disk getCurrentDisk() {
        return currentDisk;
    }
    public void setCurrentDisk(int i) {
        if (i == 1) {
            currentDisk = redDisk;
        } else if (i == -1) {
            currentDisk = yellowDisk;
        }
    }
    public void toggleCurrentDisk() {
        if (currentDisk.equals(redDisk)) {
            currentDisk = yellowDisk;
        } else {
            currentDisk = redDisk;
        }
        repaint();
    }

    public void setDiskLocation(Point p) {
        /**
         * We use Math.min and Math.max to make sure that the column doesn't lie outside of the board.
         * This way we prevent an ArrayIndexOutOfBoundsException as the Array representing the board has specific dimensions.
         */
        int selectedColumn = Math.min((int) p.getX() / columnSeparator, board.getWidth() - 1);
        selectedColumn = Math.max(selectedColumn, 0);
        Point pAdj = new Point(selectedColumn * columnSeparator + padding, 15);
        updateDisks(pAdj);
    }

    public void setDiskLocation(int column) {
        // Make sure that the disk stays within the range of the board
        column = Math.max(column, 0);
        column = Math.min(column, board.getWidth() - 1);
        updateDisks(new Point((column * columnSeparator) + padding, 15));
    }

    private void updateDisks(Point p) {
        redDisk.setLocation(p);
        yellowDisk.setLocation(p);
        repaint();
    }

    private void drawModel(Graphics g) {
        int[][] gameBoard = board.getGameBoard();
        int width = board.getWidth();
        int height = board.getHeight();
        int distanceFromTop = backgroundPaddingTop + padding;

        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int x = i * columnSeparator + padding + Math.round(i * 0.5f);
                int y = distanceFromTop + (height - (j + 1)) * columnSeparator - padding + Math.round(j * 1.3f) - Math.round((height - j) * 0.6f);
                switch (gameBoard[i][j]) {
                    case 1:
                        g.drawImage(redDiskImage, x, y, diskDiameter, diskDiameter, this);
                        break;
                    case -1:
                        g.drawImage(yellowDiskImage, x, y, diskDiameter, diskDiameter, this);
                        break;
                    case 0:
                        break;
                }
            }
        }
    }
}

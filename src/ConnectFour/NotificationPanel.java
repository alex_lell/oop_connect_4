package ConnectFour;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class NotificationPanel extends Panel {
    private JLabel banner;
    private JButton quitGame;
    private FontMetrics fontMetrics;

    public NotificationPanel(int width, int height, Controller controller) {
        super(width, height, controller);
        this.setLayout(new BorderLayout());
        this.setBackground(Color.blue);
        this.setOpaque(true);

        this.banner = new JLabel();
        this.banner.setHorizontalAlignment(SwingConstants.CENTER);
        this.banner.setFont(banner.getFont().deriveFont(35.0f).deriveFont(Font.BOLD));
        this.banner.setForeground(Color.WHITE);
        this.add(banner, BorderLayout.CENTER);

        this.quitGame = new JButton("Quit");
        this.quitGame.setFont(quitGame.getFont().deriveFont(20.0f).deriveFont(Font.BOLD));
        this.quitGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.endGame();
            }
        });
        this.add(quitGame, BorderLayout.EAST);

        this.fontMetrics = banner.getFontMetrics(banner.getFont());
    }

    public void setBanner(String text) {
        if (fontMetrics.stringWidth(text) >= width) {
            float size = width / text.length();
            banner.setFont(getFont().deriveFont((float) size * 2));
        } else {
            this.banner.setFont(banner.getFont().deriveFont(35.0f));
        }
        banner.setText(text);
        repaint();
    }
}

package ConnectFour;

import javax.swing.*;
import java.awt.Toolkit;
import java.awt.Dimension;

public class Frame extends JFrame {
    private int width;
    private int height;
    private String name;

    public Frame() {
        this.height = 800;

        // We assign a width using a 8:10 ratio between width - height.
        // This ratio we found best looking for this application.
        this.width = checkDividable((this.height / 10) * 8);


        // Check whether the monitor in use is smaller than the default size of the program
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        if (this.width > screenSize.getWidth()) {
            this.width = (int) screenSize.getWidth();
            if (this.width % 7 != 0) {
                this.width += (this.width % 7);
            }
            this.height = (this.width / 8) * 10;
        }
        if (this.height > screenSize.getHeight()) {
            this.height = (int) screenSize.getHeight() - 100;

            // We want a width that is dividable by 7. Otherwise the columnSeparator is rounded down as it is an integer.
            // This causes trouble when drawing the disks of the model.
            if ((((float) this.height / 10) * 8) % 7 != 0) {
                this.width = ((this.height / 10) * 8);
                this.width -= this.width % 7;
            } else {
                this.width = (this.height / 10 ) * 8;
            }
        }

        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.name = "Connect Four";
        this.setPreferredSize(new Dimension(this.width, this.height));
        this.pack();
        this.setTitle(this.name);
        this.setResizable(false);

        // Let the frame appear in the center of the screen.
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
    }

    // We check whether the width is dividable by 7 as there are 7 columns on the board.
    private int checkDividable(int w) {
        if (w % 7 != 0) {
            w -= w % 7;
        }
        return w;
    }

    public void setWidth(int width) {
        this.width = width;
        this.setSize(this.width, this.height);
    }
    public void setHeight(int height) {
        this.height = height;
        this.setSize(this.width, this.height);
    }
    public void setName(String name) {
        this.name = name;
        this.setTitle(this.name);
    }
    public int getWidth() {
        return width;
    }
    public int getHeight() {
        return height;
    }
    public String getName() {
        return name;
    }
}

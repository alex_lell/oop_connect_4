package ConnectFour;

import javax.swing.JPanel;
import java.awt.*;

public class Panel extends JPanel {
    protected int width;
    protected int height;
    protected Controller controller;

    public Panel(int width, int height, Controller controller) {
        this.width = width;
        this.height = height;
        this.controller = controller;
        this.setOpaque(true);
        this.setSize(this.width, this.height);
    }

    public int getWidth() {
        return width;
    }
    public int getHeight() {
        return height;
    }
}

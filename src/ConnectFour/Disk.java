package ConnectFour;

import java.awt.Image;
import java.awt.Point;

public class Disk {
    private Image image;
    private int x;
    private int y;
    private int width;
    private int height;

    public Disk(Image image, int locX, int locY, int diameter) {
        this.image = image;
        this.x = locX;
        this.y = locY;
        this.width = diameter;
        this.height = diameter;
    }

    public Image getImage() {
        return image;
    }
    public int getX() {
        return x;
    }
    public int getY() {
        return y;
    }
    public int getWidth() {
        return width;
    }
    public int getHeight() {
        return height;
    }
    public void setImage(Image image) {
        this.image = image;
    }
    public void setX(int x) {
        this.x = x;
    }
    public void setY(int y) {
        this.y = y;
    }
    public void setWidth(int width) {
        this.width = width;
    }
    public void setHeight(int height) {
        this.height = height;
    }
    public void setLocation(Point p) {
        x = (int) p.getX();
        y = (int) p.getY();
    }
}

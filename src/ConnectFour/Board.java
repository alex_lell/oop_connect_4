package ConnectFour;

import java.util.ArrayList;

public class Board {

	// width and height of the gameBoard
	private int width = 7;
	private int height = 6;

	// the internal representation of the game gameBoard
	private int[][] gameBoard = new int[width][height];

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

	public int[][] getGameBoard() {
		int[][] boardCopy = new int[width][height];

		for (int i = 0; i < width; i++) {
			boardCopy[i] = gameBoard[i].clone();
		}

		return boardCopy;
	}

	// to save the coordinates of each move
	private ArrayList<Integer> Xmoves = new ArrayList<Integer>(width * height);
	private ArrayList<Integer> Ymoves = new ArrayList<Integer>(width * height);

	/**
	 * //makeMove takes in the number of each player (1 or -1) and the column
	 * where the move is being attempted. It returns true if the move was
	 * successful and false if it was not.
	 */
	public boolean makeMove(int player, int x) {

		if (isMoveLegal(x)) {

			for (int i = 0; i < height; i++) {

				if (gameBoard[x][i] == 0) {
					gameBoard[x][i] = player;
					Xmoves.add(x); // save the coordinates of the last move
					Ymoves.add(i);
					break;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	/**
	 * isMoveLegal returns true if the move is possible and false if the last
	 * element in the column is not zero, thus if the column is full
	 */
	public boolean isMoveLegal(int x) {

		if (gameBoard[x][height - 1] == 0) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * undo removes the last move from the game gameBoard
	 */
	public boolean undo() {
		if (Xmoves.size() == 0) {
			return false;
		} else {
			gameBoard[Xmoves.get(Xmoves.size() - 1)][Ymoves
					.get(Ymoves.size() - 1)] = 0;
			Xmoves.remove(Xmoves.size() - 1);
			Ymoves.remove(Ymoves.size() - 1);
			return true;
		}
	}

	/**
	 * checkWin() searches through the gameBoard and looks whether one player
	 * has 4 tokens in a row. It returns 1 if Player 1 has 4 in a row, it
	 * returns -1 if Player 2 has 4 in a row and it returns 0 if no player has 4
	 * in a row.
	 */
	public int checkWin() {

		int vertSum, horSum, diag1Sum, diag2Sum;

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {

				vertSum = horSum = diag1Sum = diag2Sum = 0;

				if (j < height - 3) {
					vertSum = gameBoard[i][j] + gameBoard[i][j + 1]
							+ gameBoard[i][j + 2] + gameBoard[i][j + 3];
				}

				if (i < width - 3) {
					horSum = gameBoard[i][j] + gameBoard[i + 1][j]
							+ gameBoard[i + 2][j] + gameBoard[i + 3][j];
				}

				if (i < width - 3 && j < height - 3) {
					diag1Sum = gameBoard[i][j] + gameBoard[i + 1][j + 1]
							+ gameBoard[i + 2][j + 2] + gameBoard[i + 3][j + 3];

					diag2Sum = gameBoard[i][height - j - 1]
							+ gameBoard[i + 1][height - j - 2]
							+ gameBoard[i + 2][height - j - 3]
							+ gameBoard[i + 3][height - j - 4];
				}

				if (vertSum == 4 || diag2Sum == 4 || horSum == 4
						|| diag1Sum == 4) {
					return 1; // in this case, player 1 has 4 in a row, so
								// return 1
				} else if (vertSum == -4 || diag2Sum == -4 || horSum == -4
						|| diag1Sum == -4) {
					return -1; // in this case, player 2 has 4 in a row, so
								// return -1
				}
			}
		}
		return 0; // no player has 4 in a row yet
	}

	// TODO check whether it's a tie after each move. End game if true
	public boolean isTie() {
		boolean flag = true;

		for (int i = 0; i < width; i++) {
			if (isMoveLegal(i)) {
				flag = false;
				break;
			}
		}
		return flag;
	}

	// After a game is finished and restarted, the board needs to be emptied.
	public void reset() {
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				gameBoard[i][j] = 0;
			}
		}
	}


}

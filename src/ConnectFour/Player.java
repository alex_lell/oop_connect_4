package ConnectFour;

import java.awt.event.MouseAdapter;

public abstract class Player extends MouseAdapter{
    protected String name;
    protected int playerId;
    protected Board board;
    protected Controller controller;
    protected BoardPanel boardPanel;

    public Player(String name, int playerId, Board board, Controller controller) {
        this.name = name;
        this.playerId = playerId;
        this.board = board;
        this.controller = controller;
        this.boardPanel = controller.getBoardPanel();
    }

    public int getPlayerId() {
        return playerId;
    }
    public String getName() {
        return name;
    }

    public abstract void makeNextMove();
}

package ConnectFour;

public class ArtificialPlayer extends Player {

	private int width;
	private int height;

	/** max_depth determines how many layers the miniMax algorithm should go down */
	private int max_depth = 1;
									

	/** The playerId is set to -1 when the constructor is called in Controller */
	public ArtificialPlayer(String name, int playerId, Board board, Controller controller) {
		super(name, playerId, board, controller);
		width = board.getWidth();
		height = board.getHeight();
	}

    /** The difficulty can be set in the beginPanel. This function is called by the Controller-class.
     * This function changes the depth of the miniMax logarithm.
     * The depth declares how far ahead the miniMax algorithm evaluates all possible moves.
     */
    public void setMax_depth(int depth) {
        this.max_depth = depth;
        System.out.println(max_depth);
    }

    /** initialize the minValue to a very high (thus losing) value */
	public void makeNextMove() {
        int move = getMove();
		controller.setMove(move);
	}

    private int getMove(){
        double minValue = Integer.MAX_VALUE;
        int move = 0;

        for (int column = 0; column < width; column++) {
            if (board.isMoveLegal(column)) {
                double value = moveValue(column);
                if (value < minValue) {
                    minValue = value;
                    move = column;
                    if (minValue == Integer.MIN_VALUE) {
                        break;
                    }
                }
            }
        }
        return move;
    }

    /**
     * To determine the value of a move, first
     * make the move, estimate that state and
     * then undo the move again.
     * @return the column with the best winning chance
     */
	private double moveValue(int column) {
		board.makeMove(playerId, column);
		double val = alphabeta(max_depth, Integer.MIN_VALUE, Integer.MAX_VALUE,
				true);
		board.undo();
		return val;
	}

	private double alphabeta(int depth, double alpha, double beta,
			boolean maximizingPlayer) {

		// All these conditions lead to a
		// termination of the recursion
		if (depth == 0 || board.checkWin() != 0) {
			double score = evaluateBoard();

			// Note that depth in this implementation starts at a high
			// value and is decreased in every recursive call. This means that
			// the
			// deeper the recursion is, the greater max_depth - depth will
			// become and thus the smaller the result will become.
			// This is done as a tweak, simply spoken, something bad happening
			// in
			// the next turn is worse than it happening in let's say five steps.
			// Analogously something good happening in the next turn is
			// better than it happening in five steps.
			return (score / (max_depth - depth + 1));
		}

		if (maximizingPlayer) {
			for (int column = 0; column < board.getWidth(); column++) {
				if (board.isMoveLegal(column)) {
					board.makeMove(1, column);
					alpha = Math.max(alpha,
							alphabeta(depth - 1, alpha, beta, false));
					board.undo();
					if (beta <= alpha) {
						break;
					}
				}
			}
			return alpha;
		} else {
			for (int column = 0; column < board.getWidth(); column++) {
				if (board.isMoveLegal(column)) {
					board.makeMove(playerId, column);
					beta = Math.min(beta,
							alphabeta(depth - 1, alpha, beta, true));
					board.undo();
					if (beta <= alpha) {
						break;
					}
				}
			}
			return beta;
		}
	}

    /**
     * evaluateBoard evaluates the current board in terms of how many 4-, 3- or
     * 2-in-a-rows each player has in that situation. It returns
     */
	private double evaluateBoard() {

		int[][] currentBoard = board.getGameBoard();

		// In this array we save how many 3-in-a-rows and how many 2-in-a-rows
		// in the first two fields, the 3-in-a-rows of player one and two are
		// saved, in the second two fields the 2-in-a-rows of each player are stored
		int[] counter = new int[4];

		// sums is used to first 
		int[] threeSums = new int[4];
		int[] twoSums = new int[4];

		// Check if someone won. If it is the case, return a very high value
		int win = board.checkWin();
		if (win == 1) {
			return Integer.MAX_VALUE;
		} else if (win == -1) {
			return Integer.MIN_VALUE;
		}

		// count all 3-in-a-rows
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				threeSums[0] = threeSums[1] = threeSums[2] = threeSums[3] = 0;

				if (j < height - 2) {
					threeSums[0] = currentBoard[i][j] + currentBoard[i][j + 1]
							+ currentBoard[i][j + 2];
				}

				if (i < width - 2) {
					threeSums[1] = currentBoard[i][j] + currentBoard[i + 1][j]
							+ currentBoard[i + 2][j];
				}

				if (i < width - 2 && j < height - 2) {
					threeSums[2] = currentBoard[i][j] + currentBoard[i + 1][j + 1]
							+ currentBoard[i + 2][j + 2];

					threeSums[3] = currentBoard[i][height - j - 1]
							+ currentBoard[i + 1][height - j - 2]
							+ currentBoard[i + 2][height - j - 3];
				}

				for (int m : threeSums) {
					if (m == 3) {
						counter[0] += 1;
					} else if (m == -3) {
						counter[1] += 1;
					}
				}
			}
		}

		// count all 2-in-a-rows
		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				twoSums[0] = twoSums[1] = twoSums[2] = twoSums[3] = 0;

				if (j < height - 1) {
					twoSums[0] = currentBoard[i][j] + currentBoard[i][j + 1];
				}

				if (i < width - 1) {
					twoSums[1] = currentBoard[i][j] + currentBoard[i + 1][j];
				}

				if (i < width - 1 && j < height - 1) {
					twoSums[2] = currentBoard[i][j] + currentBoard[i + 1][j + 1];
					twoSums[3] = currentBoard[i][height - j - 1]
							+ currentBoard[i + 1][height - j - 2];
				}

				for (int k : twoSums) {
					if (k == 2) {
						counter[2] += 1;
					} else if (k == -2) {
						counter[3] += 1;
					}
				}
			}
		}

		// every 3-in-a-row will be counted as two 2-in-a-rows, so we subtract
		// that from the 2-in-a-rows
		counter[2] -= 2 * counter[0];
		counter[3] -= 2 * counter[1];

		// our evaluation functions weighs the number of 3-in-a-rows stronger
		// than the 2-in-a-rows by squaring them
		return (counter[0]*2*counter[0] - counter[1]*2*counter[1] + counter[2] - counter[3]);
		//return counter[0]*4 - counter[1]*4 + counter[2] - counter[3];
	}
}
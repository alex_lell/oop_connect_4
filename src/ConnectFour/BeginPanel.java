package ConnectFour;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

public class BeginPanel extends Panel {
    private JLabel welcomeLabel;
    private JLabel chooseGameMode;
    private JRadioButton singlePlayerMode;
    private JRadioButton multiPlayerMode;
    private JLabel chooseDifficulty;
    private JRadioButton difficultyEasy;
    private JRadioButton difficultyMedium;
    private JRadioButton difficultyHard;
    private JLabel firstNameInputLabel;
    private JTextField firstPlayerName;
    private JLabel secondNameInputLabel;
    private JTextField secondPlayerName;
    private JButton startButton;
    private GridBagConstraints c;

    private HashMap<JRadioButton, Integer> difficultyRange;

    private int gridBagContraintsCounter;
    private Boolean playerOneIsSet;
    private Boolean playerTwoIsSet;

    private final int depthEasy = 1;
    private final int depthMedium = 3;
    private final int depthHard = 5;

    public BeginPanel (int width, int height, final Controller controller) {
        super(width, height, controller);

        this.playerOneIsSet = false;
        this.playerTwoIsSet = false;

        // Set the layout to be able to allocate the components on the panel
        this.setLayout(new GridBagLayout());
        this.setBackground(Color.orange);
        this.setBorder(BorderFactory.createLoweredBevelBorder());

        this.welcomeLabel = new JLabel("Welcome to Connect Four");
        this.chooseGameMode = new JLabel("Choose the Game Mode");
        this.singlePlayerMode = new JRadioButton("Single Player Mode", true);
        this.multiPlayerMode = new JRadioButton("Multi-Player Mode", false);
        this.chooseDifficulty = new JLabel("Choose the Game Difficulty");
        this.difficultyEasy = new JRadioButton("Easy: Storm-Trooper-Level", true);
        this.difficultyMedium = new JRadioButton("Medium: Are you brave enough?", false);
        this.difficultyHard = new JRadioButton("Hard: Will make you cry. Brutal-mode.", false);
        this.firstNameInputLabel = new JLabel("Please fill in the name of Player 1");
        this.firstPlayerName = new JTextField();
        this.secondNameInputLabel = new JLabel("Please fill in the name of Player 2");
        this.secondPlayerName = new JTextField();
        this.startButton = new JButton("START");

        this.firstPlayerName.getDocument().putProperty("owner", firstPlayerName);
        this.secondPlayerName.getDocument().putProperty("owner", secondPlayerName);

        this.difficultyRange = new HashMap<>(3);
        this.difficultyRange.put(difficultyEasy, depthEasy);
        this.difficultyRange.put(difficultyMedium, depthMedium);
        this.difficultyRange.put(difficultyHard, depthHard);

        // Create the Listener
        ActionListener toggleGameMode = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == singlePlayerMode) {
                    controller.setIsSinglePlayer(true);
                    BeginPanel.this.toggleGameMode(true);
                } else if (e.getSource() == multiPlayerMode) {
                    controller.setIsSinglePlayer(false);
                    BeginPanel.this.toggleGameMode(false);
                }
                checkGameCanStart();
            }
        };

        ActionListener changeGameDifficulty = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JRadioButton buttonClicked = (JRadioButton) e.getSource();
                int difficulty = difficultyRange.get(buttonClicked);
                controller.setDifficulty(difficulty);
                for (JRadioButton key : difficultyRange.keySet()) {
                    if (difficultyRange.get(key) != difficulty) {
                        key.setSelected(false);
                    } else {
                        key.setSelected(true);
                    }
                }
            }
        };

        DocumentListener checkNameInput = new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                BeginPanel.this.checkNameInput(e);
                BeginPanel.this.checkGameCanStart();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                BeginPanel.this.checkNameInput(e);
                BeginPanel.this.checkGameCanStart();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                // Do nothing
            }
        };

        // Add the Listener
        this.singlePlayerMode.addActionListener(toggleGameMode);
        this.multiPlayerMode.addActionListener(toggleGameMode);
        this.difficultyEasy.addActionListener(changeGameDifficulty);
        this.difficultyMedium.addActionListener(changeGameDifficulty);
        this.difficultyHard.addActionListener(changeGameDifficulty);
        this.startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.startGame();
            }
        });

        this.firstPlayerName.getDocument().addDocumentListener(checkNameInput);
        this.secondPlayerName.getDocument().addDocumentListener(checkNameInput);


        // Some text formatting
        this.welcomeLabel.setFont(welcomeLabel.getFont().deriveFont(20.0f).deriveFont(Font.BOLD));
        this.startButton.setFont(startButton.getFont().deriveFont(16.0f).deriveFont(Font.BOLD));
        this.chooseGameMode.setFont(chooseGameMode.getFont().deriveFont(Font.BOLD));
        this.chooseDifficulty.setFont(chooseGameMode.getFont().deriveFont(Font.BOLD));
        this.welcomeLabel.setHorizontalAlignment(SwingConstants.CENTER);
        this.chooseGameMode.setHorizontalAlignment(SwingConstants.CENTER);
        this.singlePlayerMode.setHorizontalAlignment(SwingConstants.LEFT);
        this.multiPlayerMode.setHorizontalAlignment(SwingConstants.LEFT);
        this.chooseDifficulty.setHorizontalAlignment(SwingConstants.CENTER);
        this.difficultyEasy.setHorizontalAlignment(SwingConstants.LEFT);
        this.difficultyMedium.setHorizontalAlignment(SwingConstants.LEFT);
        this.difficultyHard.setHorizontalAlignment(SwingConstants.LEFT);
        this.firstNameInputLabel.setHorizontalAlignment(SwingConstants.CENTER);
        this.firstPlayerName.setHorizontalAlignment(SwingConstants.CENTER);
        this.secondNameInputLabel.setHorizontalAlignment(SwingConstants.CENTER);
        this.secondPlayerName.setHorizontalAlignment(SwingConstants.CENTER);
        this.secondNameInputLabel.setEnabled(false);
        this.secondPlayerName.setEnabled(false);
        this.startButton.setEnabled(false);

        // Specifying the location of a component and adding it the panel
        this.gridBagContraintsCounter = 0;

        this.add(welcomeLabel, getGridBagContraint(new Insets(0,0,25, 0)));
        this.add(chooseGameMode, getGridBagContraint(new Insets(0,0,5,0)));
        this.add(singlePlayerMode, getGridBagContraint(new Insets(0,0,0,0)));
        this.add(multiPlayerMode, getGridBagContraint(new Insets(0,0,15,0)));
        this.add(chooseDifficulty, getGridBagContraint(new Insets(0,0,5, 5)));
        this.add(difficultyEasy, getGridBagContraint(new Insets(0,0,0,0)));
        this.add(difficultyMedium, getGridBagContraint(new Insets(0,0,0,0)));
        this.add(difficultyHard, getGridBagContraint(new Insets(0,0,15,0)));
        this.add(firstNameInputLabel, getGridBagContraint(new Insets(0, 0, 0, 0)));
        this.add(firstPlayerName, getGridBagContraint(new Insets(0,0,5,0)));
        this.add(secondNameInputLabel, getGridBagContraint(new Insets(0,0,0,0)));
        this.add(secondPlayerName, getGridBagContraint(new Insets(0,0,15,0)));

        c = getGridBagContraint(new Insets(0,0,0,0));
        c.ipady = 10;
        this.add(startButton, c);
    }

    public String getPlayerOneName() {
        return firstPlayerName.getText();
    }
    public String getPlayerTwoName() {
        return secondPlayerName.getText();
    }

    private GridBagConstraints getGridBagContraint(Insets insets) {
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = insets;
        c.gridx = 1;
        c.gridy = gridBagContraintsCounter;
        gridBagContraintsCounter++;
        return c;
    }
    private void toggleGameMode(Boolean isSinglePlayer) {
        controller.setIsSinglePlayer(isSinglePlayer);
        singlePlayerMode.setSelected(isSinglePlayer);
        multiPlayerMode.setSelected(!isSinglePlayer);
        difficultyEasy.setEnabled(isSinglePlayer);
        difficultyMedium.setEnabled(isSinglePlayer);
        difficultyHard.setEnabled(isSinglePlayer);
        secondNameInputLabel.setEnabled(!isSinglePlayer);
        secondPlayerName.setEnabled(!isSinglePlayer);
    }
    private void checkNameInput(DocumentEvent e) {
        Color col;
        Boolean isSet;
        JTextField input = (JTextField) e.getDocument().getProperty("owner");

        if (input.getText().equals("")) {
            col = Color.red;
            isSet = false;
        } else {
            col = Color.green;
            isSet = true;
        }
        input.setBackground(col);

        if (input == firstPlayerName) {
            playerOneIsSet = isSet;
        } else if (input == secondPlayerName) {
            playerTwoIsSet = isSet;
        }
    }
    private void checkGameCanStart() {
        boolean isSinglePlayer = controller.getIsSinglePlayer();
        if (isSinglePlayer && playerOneIsSet) {
            startButton.setEnabled(true);
        } else if (!isSinglePlayer && playerOneIsSet && playerTwoIsSet) {
            startButton.setEnabled(true);
        } else {
            startButton.setEnabled(false);
        }
    }
}
package ConnectFour;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.Math;
import java.util.ArrayList;
import java.util.Random;

public class Controller extends MouseAdapter {
    private Frame frame;
    private GamePanel gamePanel;
    private NotificationPanel notificationPanel;
    private BoardPanel boardPanel;
    private BeginPanel beginPanel;
    private EndPanel endPanel;
    private Board board;
    private ArrayList<Player> listOfPlayers = new ArrayList<>(2);
    private Container pane;
    private ArtificialPlayer AI;
    private Disk currentDisk;

    private boolean isSinglePlayer = true;
    private boolean gameFinished = false;
    private int diskDiameter;
    private int columnSeparator;
    private int padding;
    private int selectedColumn;
    private Player currentPlayer;
    private Dimension usableFrameSize;
    private final int fps = 30;
    private int distance;
    private final int increment = 15;
    private Timer timer;
    private boolean animationIsRunning = false;
    private int boardHeight;
    private int boardWidth;

    /**
     * The layoutDivider describes in which proportion the notification panel and board panel are distributed on the game panel.
     * A number of e.g. 8 means that 1/8 of the game panel is assigned to the notification panel, and 7/8 to the board panel.
     */
    private final int layoutDivider = 7;

    public Controller(){
        /**
         * Create the frame with the necessary panels.
         */
        this.frame = new Frame();
        this.board = new Board();
        this.pane = frame.getContentPane();

        ActionListener timerActionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                currentDisk.setY(currentDisk.getY() + increment);
                distance -= increment;
                if (distance < 0 ) {
                    currentDisk.setY(currentDisk.getY() + increment);
                    boardPanel.repaint();
                    timer.stop();
                    animationIsRunning = false;

                    saveMove();
                    /*if (board.checkWin() == 0 && !isSinglePlayer) {
                        *//**
                         * If the player moves the mouse during an animation, the disk doesn't move to the column over which
                         * the mouse is positioned after the animation is finished.
                         * Therefore, we check the mouse location after an animation finishes and move the disk to the selected column.
                         *//*
                        double mouseLocation = MouseInfo.getPointerInfo().getLocation().getX();
                        double frameLocation = frame.getLocationOnScreen().getX();
                        int newDiskLocation = (int) (mouseLocation - frameLocation);

                        boardPanel.setDiskLocation(new Point(newDiskLocation, 15));
                    }*/
                } else {
                    boardPanel.repaint();
                }
            }
        };

        this.timer = new Timer(1000 / fps, timerActionListener);

        this.AI = new ArtificialPlayer("AI", -1, board, this);

        /**
         * The problem is that a java application is (at least in iOX) called with an application bar or menu bar that shows the application title.
         * To prevent the problem that the board is cut off at the bottom, we calculate the usable height of the frame onto which we can draw components.
         */
        this.usableFrameSize = pane.getSize();

        /**
         * The column separator is calculated using the width of the frame, the padding on the sides of the background image
         * which is around .2% of the board image, and the number of columns, in this case 7.
         */
        this.columnSeparator = Math.round((float) this.usableFrameSize.width / 7);

        /**
         * The disk diameter should be around 95% of the column separator so that the disk look like being behind the board,
         * but are still recognizable.
         */
        this.diskDiameter = Math.round(((float) this.columnSeparator / 100) * 95);
        this.padding = (this.columnSeparator - this.diskDiameter) / 2;

        this.gamePanel = new GamePanel(this.usableFrameSize.width, this.usableFrameSize.height, this);
        this.gamePanel.setBackground(Color.blue);

        this.notificationPanel = new NotificationPanel(this.gamePanel.getWidth(), (this.gamePanel.getHeight() / layoutDivider), this);
        this.boardPanel = new BoardPanel(this.gamePanel.getWidth(), (this.gamePanel.getHeight() / layoutDivider) * (layoutDivider - 1), this);
        this.beginPanel = new BeginPanel(300, 425, this);
        this.endPanel = new EndPanel(250, 200, this);

        this.gamePanel.add(endPanel);
        this.gamePanel.add(beginPanel);
        this.gamePanel.add(notificationPanel);
        this.gamePanel.add(boardPanel);

        this.notificationPanel.setLocation(0, 0);
        this.boardPanel.setLocation(0, this.gamePanel.getHeight() / layoutDivider);
        this.beginPanel.setLocation(this.gamePanel.getWidth() / 2 - this.beginPanel.getWidth() / 2, this.gamePanel.getHeight() / 2 - this.beginPanel.getHeight() / 2);
        this.endPanel.setLocation(this.gamePanel.getWidth() / 2 - this.endPanel.getWidth() / 2, this.gamePanel.getHeight() / 2 - this.endPanel.getHeight() / 2);

        this.pane.add(gamePanel);

        this.frame.setVisible(true);
        this.gamePanel.setVisible(true);
        this.notificationPanel.setVisible(false);
        this.boardPanel.setVisible(false);
        this.beginPanel.setVisible(true);
        this.endPanel.setVisible(false);
    }

    public boolean getIsSinglePlayer() {
        return isSinglePlayer;
    }

    public void setIsSinglePlayer(Boolean isSinglePlayer) {
        this.isSinglePlayer = isSinglePlayer;
    }

    public int getPadding() {
        return padding;
    }

    public int getColumnSeparator() {
        return columnSeparator;
    }

    public int getDiskDiameter() {
        return diskDiameter;
    }

    public Board getBoard() {
        return board;
    }

    public BoardPanel getBoardPanel() {
        return boardPanel;
    }

    public void setSelectedColumn(int selectedColumn) {
        this.selectedColumn = selectedColumn;
        boardPanel.setDiskLocation(selectedColumn);
    }

    public void setDifficulty(int difficulty) {
        AI.setMax_depth(difficulty);
    }

    public void setGameMode() {
        endPanel.setVisible(false);
        beginPanel.setVisible(true);
    }

    private void setKeyBindings() {
        ActionMap actionMap = boardPanel.getActionMap();
        int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
        InputMap inputMap = boardPanel.getInputMap(condition);

        String vkLeft = "37";
        String vkRight = "39";
        String vkDown = "40";

        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), vkLeft);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), vkRight);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), vkDown);

        actionMap.put(vkLeft, new KeyAction(vkLeft));
        actionMap.put(vkRight, new KeyAction(vkRight));
        actionMap.put(vkDown, new KeyAction(vkDown));
    }

    private void removeKeyBindings() {
        ActionMap actionMap = boardPanel.getActionMap();
        int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
        InputMap inputMap = boardPanel.getInputMap(condition);

        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_LEFT, 0), "none");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_RIGHT, 0), "none");
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DOWN, 0), "none");
    }

    public void mouseMoved(MouseEvent e){
        Point location = e.getPoint();
        if (!animationIsRunning) {
            boardPanel.setDiskLocation(location);
        }
    }

    public void startGame(){
        beginPanel.setVisible(false);
        endPanel.setVisible(false);
        boardPanel.addMouseMotionListener(this);
        boardPanel.requestFocusInWindow();

        setKeyBindings();

        board.reset();
        boardPanel.repaint();
        listOfPlayers.clear();

        // To keep track of which players are in the game, we add their object to an ArrayList.
        Player playerOne = new HumanPlayer(beginPanel.getPlayerOneName(), 1, board, this);


        listOfPlayers.add(playerOne);
        if (isSinglePlayer) {
            listOfPlayers.add(AI);
        } else {
            Player playerTwo = new HumanPlayer(beginPanel.getPlayerTwoName(), -1, board, this);
            listOfPlayers.add(playerTwo);
        }

        //Randomly decide, which player goes first.
        Random rnd = new Random();

        if (rnd.nextInt(2) == 0) {
            currentPlayer = listOfPlayers.get(0);
        } else {
            currentPlayer = listOfPlayers.get(1);
        }
        boardPanel.setCurrentDisk(currentPlayer.getPlayerId());
        currentDisk = boardPanel.getCurrentDisk();
        boardPanel.addMouseListener(currentPlayer);
        boardHeight = board.getHeight();
        boardWidth = board.getWidth();

        notificationPanel.setVisible(true);
        boardPanel.setVisible(true);

        if (isSinglePlayer && currentPlayer.equals(AI)) {
            notificationPanel.setBanner("AI begins the game");
        } else {
            notificationPanel.setBanner(String.format("%s, begin the game", currentPlayer.getName()));
        }
        currentPlayer.makeNextMove();
    }

    public void setMove(int selectedColumn) {
        setSelectedColumn(selectedColumn);
        animateDisk();
    }

    private void toggleCurrentPlayer() {
        if (currentPlayer.equals(listOfPlayers.get(0))) {
            currentPlayer = listOfPlayers.get(1);
        } else {
            currentPlayer = listOfPlayers.get(0);
        }
        boardPanel.toggleCurrentDisk();
        currentDisk = boardPanel.getCurrentDisk();
        currentPlayer.makeNextMove();
    }

    private void saveMove() {
        boolean saveSucceeded = board.makeMove(currentPlayer.getPlayerId(), selectedColumn);
        if (saveSucceeded) {
            boardPanel.repaint();
            if (board.isTie() || board.checkWin() != 0) {
                endGame();
            } else {
                toggleCurrentPlayer();
                toggleBannerText();
            }
        }
    }

    private void toggleBannerText() {
        if (isSinglePlayer && currentPlayer.equals(listOfPlayers.get(1))) {
            notificationPanel.setBanner("AI makes its move");
        } else {
            notificationPanel.setBanner(String.format("%s, make your move", currentPlayer.getName()));
        }
    }

    public void animateDisk() {
        int[][] gameBoard = board.getGameBoard();
        int destinationRow = -1;
        for (int i = 0; i < boardHeight; i++) {
            if (gameBoard[selectedColumn][i] == 0) {
                destinationRow = i;
                break;
            }
        }
        if (destinationRow != -1) {
            int destinationHeight = (boardHeight - destinationRow) * columnSeparator + padding;
            distance = destinationHeight - currentDisk.getY();
            timer.start();
            animationIsRunning = true;
        }
    }

    public void endGame() {
        String bannerText;
        Color bannerForeground = new Color(0, 230, 0);
        int winner = board.checkWin();

        if (board.isTie()) {
            bannerText = "Tied!";
        } else {
            if (isSinglePlayer) {
                if (winner == listOfPlayers.get(0).getPlayerId()) {
                    bannerText = "You won!";
                } else {
                    bannerText = "You lost";
                    bannerForeground = Color.red;
                }
            } else {
                if (winner == 1) {
                    bannerText = String.format("%s won!", listOfPlayers.get(0).getName());
                } else {
                    bannerText = String.format("%s won!", listOfPlayers.get(1).getName());
                }
            }
        }

        boardPanel.removeMouseMotionListener(this);
        boardPanel.removeMouseListener(this);
        removeKeyBindings();

        notificationPanel.setBanner(bannerText);
        endPanel.setBannerText(bannerText);
        endPanel.setBannerColor(bannerForeground);
        endPanel.setVisible(true);
    }

    private class KeyAction extends AbstractAction {
        public KeyAction(String actionCommand) {
            putValue(ACTION_COMMAND_KEY, actionCommand);
        }

        @Override
        public void actionPerformed(ActionEvent actionEvt) {
            int cmd = Integer.parseInt(actionEvt.getActionCommand());
            /**
             * We use the following char codes to identify which key was pressed:
             * 37 for Left Arrow Key
             * 39 for Right Arrow Key
             * 40 for Down Arrow Key
             *
             * The Left and Right Arrow Key move the disk
             * The Down Arrow Key sets the disk
             */
            if (!animationIsRunning) {
                switch (cmd) {
                    case 37:
                        setSelectedColumn(selectedColumn - 1);
                        break;
                    case 39:
                        setSelectedColumn(selectedColumn + 1);
                        break;
                    case 40:
                        setMove(selectedColumn);
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
